#!/usr/bin/env python
"""
Usage: main.py -i <file path>

Script takes a JSON file, substitutes env vars and overwrites file.
"""
import argparse
import json
import os
from pathlib import Path


def open_json_file(file_path: Path) -> dict:
    """
    Opens a JSON file.

    Args:
        file_path (Path): The path where the file will be saved

    Returns:
        file: A dictionary containing the JSON content
    """
    with open(file_path, "r") as f:
        file = json.load(f)
    return file


def save_json_file(file_path: Path, dictionary: dict) -> None:
    """
    Saves a JSON file.

    Args:
        file_path (Path): The path where the file will be saved

    Returns:
        None
    """
    with open(file_path, "w") as f:
        json.dump(dictionary, f, indent=4)


def substitute(file_content: dict, key: str = "spark_env_vars") -> dict:
    """
    Substitutes keys in a dict with env vars whose names match that key.

    Args:
        file_content (dict): The dictionary to be substituted

    Returns:
        file_content (dict): The substituted dictionary
    """
    for name in file_content[key]:
        file_content[key][name] = os.environ[name]
    return file_content


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", help="Location of input file")
    args = parser.parse_args()
    file_path = Path(args.i)
    file_content = open_json_file(file_path)
    substituted_file_content = substitute(file_content)
    save_json_file(file_path, substituted_file_content)
